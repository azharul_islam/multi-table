-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2016 at 09:44 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `practics`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_1`
--

CREATE TABLE IF NOT EXISTS `table_1` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `table_1`
--

INSERT INTO `table_1` (`id`, `name`) VALUES
(1, 'Azharul'),
(2, 'Rafsan'),
(3, 'Ovi'),
(4, 'Eyasin'),
(5, 'Muntaha');

-- --------------------------------------------------------

--
-- Table structure for table `table_2`
--

CREATE TABLE IF NOT EXISTS `table_2` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `result` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `table_2`
--

INSERT INTO `table_2` (`id`, `result`) VALUES
(1, 85),
(2, 72),
(3, 65),
(4, 70),
(5, 98);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
